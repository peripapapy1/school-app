variable "client_id" {
  description = "The Client ID which should be used for authentication."
}

variable "client_secret" {
  description = "The Client Secret which should be used for authentication."
  sensitive   = true
}

variable "subscription_id" {
  description = "The Subscription ID to deploy the resources in."
}

variable "tenant_id" {
  description = "The Tenant ID which should be used for authentication."
}
